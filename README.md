# frontend-app
This is a html project and it intends to mimic a SPA. This project exist in the ecosystem of my-nodejs-api. The purpose of this project is to upload the files inside the folder name *static* into a bucket in S3 AWS in order to try the cloudfront distribution created. 


## Deploy

This project has the built-in continuous integration in GitLab.
Therefore, to deploy the static files into the s3 bucket, you need to commit, or merge into main branch. In that sense, the jobs in the pipeline will automatically upload the files in the bucket. 

### To run it locally.
Position your terminal in the root directory of this repository. 
You should be logged in in AWS CLI and enter these commands: 

```
aws s3 rm s3://<bucket_name> --recursive
aws s3 cp static s3://<bucket_name> --recursive
```

The <bucket_name> you can get it from the outputs exposed after the *Terraform Apply* command in the [**my-nodejs-api**](https://gitlab.com/upflex1/my-nodejs-api).

***
## Authors and acknowledgment
Luis Carlos Isaula

